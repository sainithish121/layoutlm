import os
import pandas as pd
import json
import glob
from inference_handler import ModelHandler, handle
import argparse
import time
import shutil
parser = argparse.ArgumentParser()
parser.add_argument("-images_dir", "--images_dir", help = "images paths")
parser.add_argument("-annotations_dir", "--annotations_dir", help = "annotations path")
parser.add_argument("-test_image_dir", "--test_image_dir", help = "test images path")
# from Evaluate import Accuracy
# from ..GENERAL_OCR_PIPELINE.main import test_images

# from decrypting_image import main

# def run_tesseract_on_image(image_path):  # -> tsv output path
#   image_name = os.path.basename(image_path)
#   image_name = image_name[:image_name.find('.')]
#   error_code = os.system(f'''
#   tesseract "{image_path}" "/home/vishwam/mountpoint/bhanu/Inference/results/{image_name}" -l eng tsv
#   ''')
#   if not error_code:
#     return f"{image_name}.tsv"
#   else:
#     raise ValueError('Tesseract OCR Error please verify image format PNG,JPG,JPEG')


# def clean_tesseract_output(tsv_output_path):
#   path = "/home/vishwam/mountpoint/bhanu/Inference/results/"
# #   print(" here is path",tsv_output_path)
#   ocr_df = pd.read_csv(path+tsv_output_path, sep='\t', engine = "python", error_bad_lines=False)
#   ocr_df = ocr_df.dropna()
#   ocr_df = ocr_df.drop(ocr_df[ocr_df.text.str.strip() == ''].index)
#   text_output = ' '.join(ocr_df.text.tolist())
#   words = []
#   for index, row in ocr_df.iterrows():
#     word = {}
#     origin_box = [row['left'], row['top'], row['left'] +
#                   row['width'], row['top']+row['height']]
#     word['word_text'] = row['text']
#     word['word_box'] = origin_box
#     words.append(word)
#   return words


# def prepare_batch_for_inference(image_paths):
#   # tesseract_outputs is a list of paths
#   inference_batch = dict()
#   tesseract_outputs = [run_tesseract_on_image(
#       image_path) for image_path in image_paths]
#   # clean_outputs is a list of lists
#   clean_outputs = [clean_tesseract_output(
#       tsv_path) for tsv_path in tesseract_outputs]
#   word_lists = [[word['word_text'] for word in clean_output]
#                 for clean_output in clean_outputs]
#   boxes_lists = [[word['word_box'] for word in clean_output]
#                  for clean_output in clean_outputs]
#   inference_batch = {
#       "image_path": image_paths,
#       "bboxes": boxes_lists,
#       "words": word_lists
#   }
#   return inference_batch

#use tesseract+layoutlmv3 put images in testing_folder
# image_path = "/home/vishwam/mountpoint/bhanu/LayoutLMv3/dataset/testing_data/images"
# images_paths = []
# for img in  os.listdir(image_path):  
#     images_paths.append(os.path.join(image_path, img))

# infer_batch = prepare_batch_for_inference(images_paths)
# handle(infer_batch, None) 
#PREPARE TESTING DATA to feed in and CALCULATE ACCURACY.

args = parser.parse_args()
def get_testing_data ():
  annotations_path = "/home/sainithish/Downloads/may_31/test_smpl/annotations/" #args.annotations_dir
  image_folder = "/home/sainithish/Downloads/may_31/test_smpl/images/"#args.images_dir
  annotations_path_files = [i.split(".json")[0] for i in os.listdir(annotations_path)]
  image_paths = [i for i in os.listdir(image_folder) if i.split(".")[0] in annotations_path_files]

  original_box_label =  {
      "image_path": [] ,
      "bboxes": [],
      "words": [],
      "label" : []
    }

  for idx in range(len(image_paths)):

      image_name =  os.path.basename(image_paths[idx]).split(".")[0]
      original_box_label["image_path"].append(  image_folder + image_paths[idx])
      # pr
      json_file = open(annotations_path + image_name + ".json")
      json_file = json.load(json_file)
      json_file = json_file["form"]
      text_data = []
      boxes = []
      label = []
      for i in range(len(json_file)):   
        # print("Here is box", json_file[i])
        boxes.append(json_file[i]["words"][0]["box"])
        text_data.append(json_file[i]["words"][0]["text"])
        label.append(json_file[i]["label"])
      # print(" Boxes text data : ",boxes, text_data)
      original_box_label["bboxes"].append(boxes)
      original_box_label["words"].append(text_data)
      original_box_label["label"].append(label)
      
  # print(" here is testing data : ",original_box_label)
  # original_box_label =  {
  #   "image_path": [original_box_label["image_path"][0]] ,
  #   "bboxes": [original_box_label["bboxes"][0]],
  #   "words": [original_box_label["words"][0]],
  #   "label" : [original_box_label["label"][0]]
  # }

  # print("data:! " ,original_box_label )
  return original_box_label


infer_data = get_testing_data()
g = json.dumps(infer_data)
# f = open("ap_json_testing_data.json","w")
# f.write(g)
start_time = time.time()
# handle(infer_data, None) 


print("length of dictionary!",len(infer_data["image_path"]))

with open('smpl_LayoutlMV3InferenceOutput.json', mode='w', encoding='utf-8') as f:
  json.dump([], f)
count=0
for i in range(len(infer_data["image_path"])):
  try:
    original_box_label =  {

      "image_path": [infer_data["image_path"][i]],
      "bboxes": [infer_data["bboxes"][i]],
      "words": [infer_data["words"][i]],
      "label" : [infer_data["label"][i]]
    
    }
    entry = handle(original_box_label, None)
    a = []
    fname = 'smpl_LayoutlMV3InferenceOutput.json'
    if not os.path.isfile(fname):
        a.append(entry)
        with open(fname, mode='w') as f:
            f.write(json.dumps(a, indent=2))
            print("first results : ", a)
    else:
        with open(fname) as feedsjson:
            feeds = json.load(feedsjson)

        feeds.append(entry)
        with open(fname, mode='w') as f:
            f.write(json.dumps(feeds, indent=2))
            
  except Exception as e:
    count+=1
    jsonpath = os.path.basename(infer_data["image_path"][i]).split(".")[0] + ".json"
    os.remove("/home/sainithish/Downloads/may_31/test_smpl/annotations/" + jsonpath)
    print("got into except ############ ",infer_data["image_path"][i],e, i )
#   # break
infer_data1 = get_testing_data()
g = json.dumps(infer_data1)
f = open("smpl_json_testing_data.json","w")
f.write(g)

end_time = time.time()
print("total time taken : ", (end_time-start_time))
print("DONE!")

#remove the folder images testing_data
# path = "/home/vishwam/mountpoint/bhanu/LayoutLMv3/dataset/testing_data/images"                
# shutil.rmtree(path)
# calculate Edited testing data accuracy 

def get_funsdtestdata_format():
  test_annot_path = args.annotations_dir
  test_images_path  = args.images_dir
  annot_files = os.listdir(test_annot_path)

  original_box_label =  {
    "image_path": [] ,
    "bboxes": [],
    "words": [],
    "label" : []
  }
 
  for idx in range(len(annot_files)):
    text_data = []
    boxes = []
    label = []
  
    image_name =  os.path.basename(annot_files[idx]).split(".")[0]+".jpg"
    img_full_path = test_images_path + image_name
    original_box_label["image_path"].append(img_full_path)

    json_file = open(args.annotations_dir + annot_files[idx])
    json_file = json.load(json_file)
    json_file = json_file["form"]
    # print(json_file[0])
    
    for i in json_file:
      boxes.append(i["box"])
      text_data.append(i["text"])
      label.append(i["label"]) 

      for j in i["words"]:
        boxes.append(j["box"])
        text_data.append(j["text"])
        label.append(i["label"] ) 
    
    original_box_label["bboxes"].append(boxes)
    original_box_label["words"].append(text_data)
    original_box_label["label"].append(label)

  
  return original_box_label

# infer_data = get_funsdtestdata_format()
# g = json.dumps(infer_data)
# f = open("json_testing_data.json","w")
# f.write(g)
# handle(infer_data, None)
# print("DONE!")
# GENERAL_AADHAR_RESULTS + layoutlmv3

def prepare_general_ocr_data(image_folder):

  files = [i for i in os.listdir(image_folder) if i.endswith(".csv")]
  inference_data = {
    "image_path": [] ,
    "bboxes": [],
    "words": []
  }
  for fl in files:
    df_result = pd.read_csv(os.path.join(image_folder,fl))
    text_data = []
    boxes = []
    for cord, word in zip(df_result["bbox"],df_result["prediction"]):
      # print("here are the results : ", i, j)
      fres = [k.replace("'","") for k  in  cord.strip('][').split(',')]
      fres = list(map(int,fres))
      boxes.append(fres)
      text_data.append(word)
      
    image_path = os.path.join(image_folder,fl.split(".")[0]+".jpg") #. should not be present in tsv file name
    inference_data["words"].append(text_data)
    inference_data["bboxes"].append(boxes)
    inference_data["image_path"].append(image_path)
  # print("this is data ", inference_data)
  return inference_data

# image_folder = "/home/vishwam/mountpoint/bhanu/GENERAL_OCR_PIPELINE/testing_data_ap/"
# infer_data = prepare_general_ocr_data(image_folder)

# g = json.dumps(infer_data)
# f = open("json_testing_data.json","w")
# f.write(g)
# handle(infer_data, None)
# print("DONE!")

#calculate Accracy
# Accuracy()

