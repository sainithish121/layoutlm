import json
import os
import numpy as np
def Accuracy():
    json_fl = open("/home/sainithish/Downloads/poi test/smpl_LayoutlMV3InferenceOutput.json")
    results = json.load(json_fl)
    image_paths = open("/home/sainithish/Downloads/poi test/smpl_json_testing_data.json")
    img_paths  = json.load(image_paths)
    predicted_box_label = {} 
    confidence_mean = []
    for idx in range(len(results)):
        # print("################", os.path.basename(img_paths["image_path"][idx]))
        image_name =  os.path.basename(img_paths["image_path"][idx]).split(".")[0]
        output = results[idx]["output"]
        boxes_label = []
        for i in range(len(output)):
            label =  output[i]["label"]
            
            for j in range(len(output[i]["words"])):
                boxes = output[i]["words"][j]["box"]
                text = output[i]["words"][j]["text"]
                try:
                    conf = output[i]["words"][j]["conf"]
                    
                except:
                    conf =  output[i]["conf"]  
                confidence_mean.append(conf)
                # print(boxes,label, text, conf)
                boxes_label.append((boxes,label, text, conf))

        # print((boxes_label) ) 
        # print()   
        boxes_label = sorted(boxes_label, key = lambda x : x[0][1])
        predicted_box_label[image_name] = boxes_label
    # print("mean of confidence is : minimum : maximum : ",np.mean(confidence_mean), max(confidence_mean), min(confidence_mean))
    image_paths = open("/home/sainithish/Downloads/poi test/smpl_json_testing_data.json")
    img_paths  = json.load(image_paths)
    confidence_mean = []
    original_box_label = {}
    for idx in range(len(img_paths["image_path"])):
        box_label = []
        image_name =  os.path.basename(img_paths["image_path"][idx]).split(".")[0]
    #print("image name is : ", image_path)
        json_file = open("/home/sainithish/Downloads/may_31/test_smpl/annotations/" + image_name + ".json")
        json_file = json.load(json_file)
        json_file = json_file["form"]
        for i in range(len(json_file)):   
            box = json_file[i]["words"][0]["box"]
            label = json_file[i]["label"]
            text = json_file[i]["words"][0]["text"]
            box_label.append((box, label, text))
        box_label = sorted(box_label, key = lambda x : x[0][1] )
        original_box_label[image_name] = box_label
    # print((predicted_box_label[image_name]))
    # print(len(output[i]["label"]))
    count = 0 
    correct=0
    total = 0
    # fname_correct     = 0
    # fname_incorrect   = 0
    name_correct      = 0
    name_incorrect    = 0
    address_correct   = 0
    address_incorrect = 0
    dl_no_correct   = 0
    dl_no_incorrect = 0
    # fname_count = 0
    name_count =  0
    address_count = 0
    dl_no_count = 0
    dob_incorrect = 0
    dob_correct = 0
    dob_count   = 0

    heading_incorrect = 0
    heading_correct = 0
    heading_count   = 0

    doi_incorrect = 0
    doi_correct = 0
    doi_count   = 0

    state_count = 0
    state_correct = 0
    state_incorrect = 0

    guardian_count=0
    guardian_correct=0
    guardian_incorrect=0

    valid_date_count=0
    valid_date_correct=0
    valid_date_incorrect=0

    blood_group_count=0
    blood_group_correct=0
    blood_group_incorrect=0
    
    temp_address_count=0
    temp_address_correct=0
    temp_address_incorrect=0

    licencing_authority_count=0
    licencing_authority_correct=0
    licencing_authority_incorrect=0

    undefined_count=0
    undefined_correct=0
    undefined_incorrect=0
    unq=[]
    dictt={}
    fc=[]
    # org=[]
    # pred=[]
    # n_org=[]
    # n_pred=[]
    # for i  in original_box_label:
    #     print("*************",i)
    #     count+=1 
    #     print("original ----------->>> ",original_box_label[i],len(original_box_label[i]))
    #     print("pridicted ---------->>> ",predicted_box_label[i],len(predicted_box_label[i]))
    # #     #break
        # for k in range(len(predicted_box_label[i])):
        #     pred.append(predicted_box_label[i][k])
        # for k in range(len(original_box_label[i])):
        #     org.append(original_box_label[i][k])
        # # print(len(org))
        # for k in pred:
        #     for j in org:
        #         if (k[0]==j[0]):
        #             n_org.append(j)
        #             n_pred.append(k)

        # print('after pred_idex corr ',len(n_pred))
        # print('after org_idex corr ',len(n_org))

        
        # print(len(org))
        
        
        
         
                       
    for i  in original_box_label:
        print("*************img",i)
        count+=1 
        # print("original ----------->>> ",original_box_label[i],len(original_box_label[i]))
        # print("pridicted ---------->>> ",predicted_box_label[i],len(predicted_box_label[i]))
        #break
        org=[]
        pred=[]
        n_org=[]
        n_pred=[]

        for k in range(len(predicted_box_label[i])):
            pred.append(predicted_box_label[i][k])
        for k in range(len(original_box_label[i])):
            org.append(original_box_label[i][k])
        # print(len(org))
        for k in org:
            for j in pred:
                if (k[0]==j[0]):
                    n_org.append(k)
                    n_pred.append(j)

        print('after pred_idex corr ',len(n_pred))
        print('after org_idex corr ',len(n_org))

        for idx in range(len(n_org)):
            if (n_org[idx][1]) not in list(dictt.keys()):
                dictt[n_org[idx][1]]=1
            else:
                dictt[n_org[idx][1]]=dictt[n_org[idx][1]]+1
            unq.append(n_org[idx][1])

            # try:
            if True:
                if n_org[idx][1]!="other" :  
                    total+=1

                    # if str(n_org[idx][2]).lower()== str(n_pred[idx][2]).lower(): 
                    if (n_org[idx][0])== (n_pred[idx][0]): 
                        if n_org[idx][1]== n_pred[idx][1]:                 
                            correct += 1
                        elif (n_org[idx][1] in ['valid_date','valid date','validity date']) and ( n_pred[idx][1] in ['valid_date','valid date','validity date']):                 
                            correct += 1
                        else:
                            print("original, predicted : ", n_org[idx][1],n_pred[idx][1],n_pred[idx][2])
                # print(n_org[idx][1])
                if str(n_org[idx][1]).lower() == "guardian":  #fname
                    # print("*************", n_pred[idx][3])
                    guardian_count+=1
                    if  n_pred[idx][1]==n_org[idx][1]:
                        guardian_correct+=1
                    else:
                        guardian_incorrect+=1
                    # if n_pred[idx][3]>27:
                    #     fname_count+=1
                    #     print("============",n_pred[idx][1] , n_org[idx][1], n_pred[idx][3])
                    #     if n_pred[idx][1]==n_org[idx][1]:
                    #         fname_correct+=1
                    #     else:
                    #         fname_incorrect+=1
                elif str(n_org[idx][1]).lower() == "name" :
                    
                    name_count+=1
                    if  n_pred[idx][1]==n_org[idx][1]:
                        name_correct+=1
                    else:
                        name_incorrect+=1
                elif (str(n_org[idx][1]).lower() == "address") :
                   
                    address_count+=1
                    if n_pred[idx][1]==n_org[idx][1]:
                        address_correct +=1
                    else:
                        # fc.append(i)
                        address_incorrect+=1

                elif str(n_org[idx][1]).lower() == "dob":
                    dob_count+=1
                    if n_pred[idx][1]==n_org[idx][1]:
                        dob_correct +=1
                    else:
                        dob_incorrect+=1
                
                elif str(n_org[idx][1]).lower() == "doi":
                    doi_count+=1
                    if n_pred[idx][1]==n_org[idx][1]:
                        doi_correct +=1
                    else:
                        doi_incorrect+=1

                elif str(n_org[idx][1]).lower() == "state":
                    state_count+=1
                    if n_pred[idx][1]==n_org[idx][1]:
                        state_correct +=1
                    else:
                        state_incorrect+=1
                
                elif str(n_org[idx][1]).lower() == "dl_no" :
                   
                    dl_no_count+=1
         
                    if n_pred[idx][1]==n_org[idx][1]:
                        dl_no_correct+=1
                    else:
                        dl_no_incorrect+=1
                
                elif str(n_org[idx][1]).lower() == "heading" :
                   
                    heading_count+=1
         
                    if n_pred[idx][1]==n_org[idx][1]:
                        heading_correct+=1
                    else:
                        heading_incorrect+=1

                elif ((str(n_org[idx][1]).lower()) in ['valid_date','valid date','validity date']) or ((str(n_org[idx][1]).lower()) in ['valid_date','valid date','validity date']):
                    print(n_org[idx][1],n_pred[idx][1])
                    valid_date_count+=1
         
                    if n_pred[idx][1]==n_org[idx][1]:
                        valid_date_correct+=1
                    elif (n_org[idx][1] in ['valid_date','valid date','validity date']) and (n_pred[idx][1] in ['valid_date','valid date','validity date']):  
                        valid_date_correct+=1
                    else:
                        valid_date_incorrect+=1
                
                elif str(n_org[idx][1]).lower() == "blood group" :
                   
                    blood_group_count+=1
         
                    if n_pred[idx][1]==n_org[idx][1]:
                        blood_group_correct+=1
                    else:
                        blood_group_incorrect+=1

                elif str(n_org[idx][1]).lower() == "temp.address" :
                    print('tmp',n_org[idx][1],n_pred[idx][1])
                    temp_address_count+=1
         
                    if n_pred[idx][1]==n_org[idx][1]:
                        temp_address_correct+=1
                    else:
                        temp_address_incorrect+=1

                elif str(n_org[idx][1]).lower() == "licencing authority" :
                   
                    licencing_authority_count+=1
         
                    if n_pred[idx][1]==n_org[idx][1]:
                        licencing_authority_correct+=1
                    else:
                        licencing_authority_incorrect+=1

                elif str(n_org[idx][1]).lower() == "undefined" :
                   
                    undefined_count+=1
         
                    if n_pred[idx][1]==n_org[idx][1]:
                        undefined_correct+=1
                    else:
                        undefined_incorrect+=1

                else:
                    fc.append(n_org[idx][1])
                    # print()
                    # print(f" {str(n_org[idx][2]).lower()} Label not counted ")
                    
            # except:
            #     pass
            
    if total>0:
        print("total boxes, correct boxes, Accuracy : ",  total, correct, round(correct/total*100,2))
    if guardian_count>0:
        print("guardian total, correct, incorrect, accuracy :  ", guardian_count, guardian_correct, guardian_incorrect, round(guardian_correct/guardian_count * 100, 2))
    if name_count>0:
        print("name total, correct , incorrect, accuracy :  ", name_count, name_correct, name_incorrect, round(name_correct/name_count * 100, 2))
    if address_count>0:
        print("address total, correct, incorrect , accuracy :  ", address_count, address_correct, address_incorrect, round(address_correct/address_count * 100, 2))
    if dob_count>0:
        print("dob total, correct, incorrect , accuracy :  ", dob_count, dob_correct, dob_incorrect, round(dob_correct/dob_count * 100, 2))
    if doi_count>0:
        print("doi total, correct, incorrect , accuracy :  ", doi_count, doi_correct, doi_incorrect, round(doi_correct/doi_count * 100, 2))
    if state_count>0:
        print("state total, correct, incorrect , accuracy :  ", state_count, state_correct, state_incorrect, round(state_correct/state_count * 100, 2))
    if dl_no_count>0:
        print("dl_no total, correct, incorrect , accuracy :  ", dl_no_count, dl_no_correct, dl_no_incorrect, round(dl_no_correct/dl_no_count * 100, 2))

    if heading_count>0:
        print("heading total, correct, incorrect , accuracy :  ", heading_count, heading_correct, heading_incorrect, round(heading_correct/heading_count * 100, 2))
        
    if valid_date_count>0:
        print("valid_date total, correct, incorrect , accuracy :  ", valid_date_count, valid_date_correct, valid_date_incorrect, round(valid_date_correct/valid_date_count * 100, 2))

    if blood_group_count>0:
        print("blood_group total, correct , incorrect, accuracy :  ", blood_group_count, blood_group_correct, blood_group_incorrect, round(blood_group_correct/blood_group_count * 100, 2))

    if temp_address_count>0:
        print("temp_address total, correct , incorrect, accuracy :  ", temp_address_count, temp_address_correct, temp_address_incorrect, round(temp_address_correct/temp_address_count * 100, 2))
    if licencing_authority_count>0:
        print("licencing_authority total, correct, incorrect, accuracy :  ", licencing_authority_count, licencing_authority_correct, licencing_authority_incorrect, round(licencing_authority_correct/licencing_authority_count * 100, 2))
    if undefined_count>0:
        print("undefined total, correct, incorrect , accuracy :  ", undefined_count, undefined_correct, undefined_incorrect, round(undefined_correct/undefined_count * 100, 2))

    print(" ")
    print("Labels: ",set(unq))
    print(" ")
    print("manual counts:",dictt)
    cnt=0
    for i in dictt.values():
        cnt=cnt+i
    print('dict_count: ',cnt)
    print("Final Total:",cnt-dictt['other'])
    print(" ")
    print('else')
    print(set(fc))
Accuracy()



