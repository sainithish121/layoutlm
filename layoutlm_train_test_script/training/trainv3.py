from datasets.features import ClassLabel
from datasets import load_dataset
from transformers import AutoProcessor
import os
#from decrypting_images import main

os.environ["CUDA_VISIBLE_DEVICES"]="0,1,2,3"

ckpt_folder = "dl_poa_trained_checkpoint_v3" #"DL_POA_trained_checkpoint_v3"
training_data = load_dataset("load_training_data_v3.py")
training_data = training_data["train"]
testing_data = load_dataset("load_testing_data_v3.py")
testing_data = testing_data["test"]

features = training_data.features
column_names = training_data.column_names
image_column_name = "image"
text_column_name = "tokens"
boxes_column_name = "bboxes"
label_column_name = "ner_tags"
processor = AutoProcessor.from_pretrained("microsoft/layoutlmv3-base", apply_ocr=False)
# In the event the labels are not a `Sequence[ClassLabel]`, we will need to go through the dataset to get the
# unique labels.
def get_label_list(labels):
    unique_labels = set()
    for label in labels:
        unique_labels = unique_labels | set(label)
    label_list = list(unique_labels)
    label_list.sort()
    return label_list

if isinstance(features[label_column_name].feature, ClassLabel):
    label_list = features[label_column_name].feature.names
    # No need to convert the labels since they are already ints.
    id2label = {k: v for k,v in enumerate(label_list)}
    label2id = {v: k for k,v in enumerate(label_list)}
    print("id2label",id2label)
    print("id2label",label2id)
else:
    label_list = get_label_list(dataset["train"][label_column_name])
    id2label = {k: v for k,v in enumerate(label_list)}
    label2id = {v: k for k,v in enumerate(label_list)}
num_labels = len(label_list)

def prepare_examples(examples):
  images = examples[image_column_name]
  words = examples[text_column_name]
  boxes = examples[boxes_column_name]
  word_labels = examples[label_column_name]

  encoding = processor(images, words, boxes=boxes, word_labels=word_labels,
                       truncation=True, padding="max_length")

  return encoding

from datasets import Features, Sequence, ClassLabel, Value, Array2D, Array3D

# we need to define custom features for `set_format` (used later on) to work properly
features = Features({
    'pixel_values': Array3D(dtype="float32", shape=(3, 224, 224)),
    'input_ids': Sequence(feature=Value(dtype='int64')),
    'attention_mask': Sequence(Value(dtype='int64')),
    'bbox': Array2D(dtype="int64", shape=(512, 4)),
    'labels': Sequence(feature=Value(dtype='int64')),
})

train_dataset = training_data.map(
    prepare_examples,
    batched=True,
    remove_columns=column_names,
    features=features,
)
eval_dataset = testing_data.map(
    prepare_examples,
    batched=True,
    remove_columns=column_names,
    features=features,
)

import numpy as np
return_entity_level_metrics = False
from datasets import load_metric

metric = load_metric("seqeval")
def compute_metrics(p):
    # predictions, labels = p
    # predictions = np.argmax(predictions, axis=2)
    # # Remove ignored index (special tokens)
    # true_predictions = [
    #     [label_list[p] for (p, l) in zip(prediction, label) if l != -100]
    #     for prediction, label in zip(predictions, labels)
    # ]
    # true_labels = [
    #     [label_list[l] for (p, l) in zip(prediction, label) if l != -100]
    #     for prediction, label in zip(predictions, labels)
    # ]

    # results = metric.compute(predictions=true_predictions, references=true_labels)
    # if return_entity_level_metrics:
    #     # Unpack nested dictionaries
    #     final_results = {}
    #     for key, value in results.items():
    #         if isinstance(value, dict):
    #             for n, v in value.items():
    #                 final_results[f"{key}_{n}"] = v
    #         else:
    #             final_results[key] = value
    #     return final_results
    # else:
        return {
            "precision": 0.99,    #results["overall_precision"],
            "recall": 0.99, #results["overall_recall"],
            "f1": 0.99, #results["overall_f1"],
            "accuracy": 0.99, #results["overall_accuracy"],
        }
    
    
from transformers import LayoutLMv3ForTokenClassification

model = LayoutLMv3ForTokenClassification.from_pretrained("microsoft/layoutlmv3-base",
                                                         id2label=id2label,
                                                         label2id=label2id)


from transformers import TrainingArguments, Trainer
training_args = TrainingArguments(output_dir=ckpt_folder,
                                  max_steps=50000,
                                  per_device_train_batch_size=4,
                                  per_device_eval_batch_size=4,
                                  learning_rate=1e-6,
                                  evaluation_strategy="steps",
                                  eval_steps=500,
                                  load_best_model_at_end=True,
                                  metric_for_best_model=None)



from transformers.data.data_collator import default_data_collator
# Initialize our Trainer
import shutil

# path = "/home/vishwam/mountpoint/bhanu/LayoutLMv3/dataset/testing_data/images"                
# shutil.rmtree(path)
# path = "/home/vishwam/mountpoint/bhanu/LayoutLMv3/dataset/training_data/images"                
# shutil.rmtree(path)

trainer = Trainer(
    model=model,
    args=training_args,
    train_dataset=train_dataset,
    eval_dataset=eval_dataset,
    tokenizer=processor,
    data_collator=default_data_collator,
    compute_metrics=None,
)

trainer.train()
trainer.save_model()
trainer.evaluate()