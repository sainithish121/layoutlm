import ctypes
import base64
import os
# decrypt = ctypes.cdll.LoadLibrary('/data/dkyc_helper/decrypt_ubuntu1804.so')
key = ctypes.cdll.LoadLibrary("/home/sainithish/Downloads/layoutlm/layout_test_harness/decrypt_ubuntu2004.so")
# # get the decryptImage function & assign to python function
decrypt_img = key.decryptImage
# # set decrypt function input & return types
decrypt_img.argtypes = [ctypes.c_char_p]
decrypt_img.restype = ctypes.c_void_p


def decrypt_image_train(decrypt_img, image_dir_path):
    rawp = decrypt_img(image_dir_path.encode('utf-8'))
    raw_bytes = ctypes.string_at(rawp)
    raw_string = raw_bytes.decode('utf-8')
    # convert base64 string to obtain decrypted imag
    imgdata = base64.b64decode(raw_string)
    print()
    # print("Image base64 string in Python:",len(raw_string))
    #saving file for reference
    filename =  "dataset/training_data/images/" + os.path.basename(image_dir_path).split(".")[0]+".png"
    with open(filename,'wb') as f:
        f.write(imgdata)   

def decrypt_image_test(decrypt_img, image_dir_path):
    rawp = decrypt_img(image_dir_path.encode('utf-8'))
    raw_bytes = ctypes.string_at(rawp)
    raw_string = raw_bytes.decode('utf-8')
    # convert base64 string to obtain decrypted imag
    imgdata = base64.b64decode(raw_string)
    # print("Image base64 string in Python:",len(raw_string))
    #saving file for reference
    filename =  "dataset/testing_data/images/"  + os.path.basename(image_dir_path).split(".")[0]+".png"
    with open(filename,'wb') as f:
        f.write(imgdata)   


img_path = "/home/sainithish/Downloads/layoutlm/layout_test_harness/dataset/training_data/images_encrypt/"
os.makedirs("dataset/training_data/images")
for path in os.listdir(img_path):
    decrypt_image_train(decrypt_img, img_path+path)


img_path = "/home/sainithish/Downloads/layoutlm/layout_test_harness/dataset/testing_data/images_encrypt/"
os.makedirs("dataset/testing_data/images/" )
for path in os.listdir(img_path):
    decrypt_image_test(decrypt_img, img_path+path)


