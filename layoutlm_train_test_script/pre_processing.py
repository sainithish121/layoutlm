import json,os
from ast import literal_eval
import pandas as pd 
import numpy as np

#path_to_save_jsons
json_path="/content/karnataka/"
#csv_path_from_kushi_labelling
b=pd.read_csv("/content/tamil_nadu_kerala_final.csv",converters={'boxes': literal_eval})

def write_json(data,filename,indent):
    with open(filename, 'w') as f:
        json.dump(data,f,indent=indent)

for i in range(len(np.unique(b["image"]))):
  print(np.unique(b["image"])[i])
  d=b[b["image"]==np.unique(b["image"])[i]].reset_index()
  print(d.columns)
  d=d[["prediction","boxes","Labels","image"]]
  output_json = json_path+np.unique(b["image"])[i].replace(".jpg",".json")
  print(output_json)
  form = {
          "form": 
                  [
                    
                  ]
         }
  write_json(form,output_json,4)

  for j in range(len(d)):
    dictionary={"box":d.iloc[j,1],"text":d.iloc[j,0],"label":d.iloc[j,2],"words":[{"box":d.iloc[j,1],"text":d.iloc[j,0]}],"linking": [],"id":j}
    print(dictionary)
  #list_of_dicts.append(dictionary)
    with open(output_json) as outfile:
      data=json.load(outfile)
      temp = data["form"]
      temp.append(dictionary)
      write_json(data,output_json,4)
